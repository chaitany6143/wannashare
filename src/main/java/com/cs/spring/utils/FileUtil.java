package com.cs.spring.utils;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import org.jboss.logging.Logger;

public class FileUtil {
	
	static Logger log = Logger.getLogger(FileUtil.class.getName());

	public static String createFolderTree(String userid, String postid) {
		
		
		String tmpDirStr = System.getProperty("java.io.tmpdir");
	    if (tmpDirStr == null) {
	      try {
			throw new IOException(
			    "System property 'java.io.tmpdir' does not specify a tmp dir");
		} catch (IOException e) {
			log.error(e.getLocalizedMessage());
		}
	    }
	    
	    File tmpDir = new File(tmpDirStr);
	    if (!tmpDir.exists()) {
	      boolean created = tmpDir.mkdirs();
	      if (!created) {
	        try {
				throw new IOException("Unable to create tmp dir " + tmpDir);
			} catch (IOException e) {
				log.error(e.getLocalizedMessage());
			}
	      }
	    }
	   String destinationPath=tmpDirStr+"/"+Constants.APP_BASE_DIR+"/"+userid+"/"+postid;
	    File destinationDir = new File(destinationPath);
	    if(!destinationDir.exists()){
	    	boolean created = destinationDir.mkdirs();
	    if (!created) {
	        try {
				throw new IOException("Unable to create tmp dir " + tmpDir);
			} catch (IOException e) {
				log.error(e.getLocalizedMessage());
			}
	      }
		
	}
	    return destinationPath;
	    }
	
	public static boolean createDirectories(String path){
		File destinationDir = new File(path);
	    if(!destinationDir.exists()){
	    	boolean created = destinationDir.mkdirs();
	    if (!created)
	    	return false;
	    }
	
	    return true;	
	}

	public static List<String> getFilesFromFolderTree(String userid, String postid, String type) {
		List<String> returnList = new ArrayList<>();
		String tmpDirStr = System.getProperty("java.io.tmpdir");
	    if (tmpDirStr == null) {
	      try {
			throw new IOException(
			    "System property 'java.io.tmpdir' does not specify a tmp dir");
		} catch (IOException e) {
			log.error(e.getLocalizedMessage());
		}
	    }
	    
	    File tmpDir = new File(tmpDirStr);
	    
	   String destinationPath=tmpDirStr+"/"+Constants.APP_BASE_DIR+"/"+userid+"/"+postid+"/"+type;
	    File destinationDir = new File(destinationPath);
	   if(destinationDir.listFiles()!=null){
	    for(File file: destinationDir.listFiles()){
	    	returnList.add(file.getAbsolutePath());
	    	
	    };
	   }
	 return returnList;   
		
	}
	public static File compressJpegFile(File infile, File outfile, float compressionQuality) {
	    try {
	        // Retrieve jpg image to be compressed
	        RenderedImage rendImage = ImageIO.read(infile);
	 
	        // Find a jpeg writer
	        ImageWriter writer = null;
	        Iterator iter = ImageIO.getImageWritersByFormatName("jpg");
	        if (iter.hasNext()) {
	            writer = (ImageWriter)iter.next();
	        }
	 
	        // Prepare output file
	        ImageOutputStream ios = ImageIO.createImageOutputStream(outfile);
	        writer.setOutput(ios);
	 
	        // Set the compression quality
	        ImageWriteParam iwparam = new MyImageWriteParam();
	        iwparam.setCompressionMode(ImageWriteParam.MODE_DEFAULT) ;
	        iwparam.setCompressionQuality(compressionQuality);
	 
	        // Write the image
	        writer.write(null, new IIOImage(rendImage, null, null), iwparam);
	 
	        // Cleanup
	        ios.flush();
	        writer.dispose();
	        ios.close();
	    } catch (IOException e) {
	    }
	    return outfile;
	}
	 
	// This class overrides the setCompressionQuality() method to workaround
	// a problem in compressing JPEG images using the javax.imageio package.
	private static class MyImageWriteParam extends JPEGImageWriteParam {
	    public MyImageWriteParam() {
	        super(Locale.getDefault());
	    }
	 
	    // This method accepts quality levels between 0 (lowest) and 1 (highest) and simply converts
	    // it to a range between 0 and 256; this is not a correct conversion algorithm.
	    // However, a proper alternative is a lot more complicated.
	    // This should do until the bug is fixed.
	    public void setCompressionQuality(float quality) {
	        if (quality < 0.0F || quality > 1.0F) {
	            throw new IllegalArgumentException("Quality out-of-bounds!");
	        }
	        this.compressionQuality = 256 - (quality * 256);
	    }
	}

	
}
