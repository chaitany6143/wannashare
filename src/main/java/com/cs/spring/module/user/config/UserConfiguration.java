package com.cs.spring.module.user.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.cs.spring.module.user.dao.UserDAO;
import com.cs.spring.module.user.dao.UserDAOImpl;
import com.cs.spring.module.user.model.User;
import com.cs.spring.module.user.service.UserService;
import com.cs.spring.module.user.service.UserServiceImpl;

@Configuration
@ComponentScan(basePackages = "com.cs.spring")
public class UserConfiguration {

	/*@Autowired
	@Bean(name = "userService")
	public UserService getUserService()
	{
		return new UserServiceImpl();
	}

	@Autowired
	@Bean(name = "userDAO")
	public UserDAO getUserDao()
	{
		return new UserDAOImpl();
	}
	
	@Autowired
	@Bean(name = "userDetailsDAO")
	public UserDetailsDAO getUserDetailsDao()
	{
		return new UserDetailsDAOImpl();
	} */
}
