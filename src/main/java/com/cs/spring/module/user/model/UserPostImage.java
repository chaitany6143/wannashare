package com.cs.spring.module.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 
 * @author Chaitanya
 *
 */
@Entity

@Table(name="app_post_images_data")
public class UserPostImage {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int imageid;
	
	
	private String postid;
	
	private String actualloc;
	
	private String thumbnailloc;
	
	private String filename;


	public int getImageid() {
		return imageid;
	}

	public void setImageid(int imageid) {
		this.imageid = imageid;
	}

	public String getPostid() {
		return postid;
	}

	public void setPostid(String postid) {
		this.postid = postid;
	}

	public String getActualloc() {
		return actualloc;
	}

	public void setActualloc(String actualloc) {
		this.actualloc = actualloc;
	}

	public String getThumbnailloc() {
		return thumbnailloc;
	}

	public void setThumbnailloc(String thumbnailloc) {
		this.thumbnailloc = thumbnailloc;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
}
