package com.cs.spring.module.user.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cs.spring.module.user.model.PostComment;
import com.cs.spring.module.user.model.User;
import com.cs.spring.module.user.model.Post;
import com.cs.spring.module.user.model.UserPostResponse;
import com.cs.spring.module.user.model.UserPostImage;
import com.cs.spring.module.user.model.UserPostLike;

@Repository
public class UserDAOImpl implements UserDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addUser(User p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(p);
		logger.info("User saved successfully, Person Details="+p);
	}

	@Override
	public void updateUser(User p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("User updated successfully, Person Details="+p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUsers() {
		Session session = this.sessionFactory.getCurrentSession();
		List<User> personsList = session.createQuery("from User").list();
		for(User p : personsList){
			logger.info("User List::"+p);
		}
		return personsList;
	}

	@Override
	public User getUserById(String id) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from app_user where userid=?");
		query.setParameter(0,id);
		query.addEntity(User.class);
		return (User) query.list().get(0);
		
	}

	@Override
	public void removeUser(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		User p = (User) session.load(User.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("User deleted successfully, person details="+p);
	}

	@Override
	public boolean isUserExist(String userid) {
		Session session = this.sessionFactory.getCurrentSession();
		Query q = session.createQuery("from User where userid = :userid");
		q.setParameter("userid", userid);       
		User e = (User)q.uniqueResult();
		if(e==null)
		return false;
		else
			return true;
	}

	
	@Override
	public int saveUserPost(Post userPost) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(userPost);
		
		logger.info("User post saved successfully,Details="+userPost);
		return userPost.getPostid();
		
	}

	@Override
	public void saveUserPostImage(UserPostImage userPostImage) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(userPostImage);
		logger.info("User post image saved successfully,Details="+userPostImage); 
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> getUserPosts(String userid) {
		
			Session session = this.sessionFactory.getCurrentSession();
			Query q = session.createQuery("from UserPost where userid = :userid");
			q.setParameter("userid", userid);
			List<Post> userPostList=(List<Post>)q.list();
			
			return userPostList;
		}

	@Override
	public List<UserPostResponse> getNearbyPosts() {
		UserPostResponse userPost;
		List<UserPostResponse> userPostList = new ArrayList<UserPostResponse>();
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from app_user_post");
		
		query.addEntity(Post.class);
		List<Post> postList = query.list();
		for(Post post : postList){
			SQLQuery userQuery = session.createSQLQuery("select * from app_user where userid=?");
			userQuery.setParameter(0,post.getUserid());
			userQuery.addEntity(User.class);
			
			userPost = new UserPostResponse();
			userPost.setPost(post);
			userPost.setUser((User)userQuery.list().get(0));
			
			/*SQLQuery userImageQuery = session.createSQLQuery("select * from app_post_images_data where postid=?");
			userImageQuery.setParameter(0,post.getPostid());
			userImageQuery.addEntity(UserPostImage.class);
			userPost.setUserPostImage(userImageQuery.list());  */
			userPostList.add(userPost);
			
			
		}

		logger.info("Session loaded successfully");
		return userPostList;
	}

	@Override
	public User getUserByPostId(int postid) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select userid from app_user_post where postid=?");
		query.setParameter(0,postid);
		List userIdList = query.list();
		String userId =(String)userIdList.get(0);
		User user = getUserById(userId);
		return user;
	}

	@Override
	public int savePostComment(PostComment postComment) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(postComment);
		
		logger.info(" Post comment saved successfully,Details="+postComment);
		return postComment.getCommentid();
	}

	@Override
	public List<PostComment> getAllPostComments(String postid) {
		
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from app_user_post_comment where postid=?");
		query.setParameter(0,postid);
		query.addEntity(PostComment.class);
		List<PostComment> postCommentList = (List<PostComment>)query.list();
		
		return postCommentList;
	}

	@Override
	public boolean addPostLike(UserPostLike postLike) {
		
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(postLike);
		logger.info(" Post comment saved successfully,Details="+postLike);
		return true;
	}

	@Override
	public List<UserPostLike> getAllPostLikes(String postid) {
		
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from app_user_post_like where postid=?");
		query.setParameter(0,postid);
		query.addEntity(UserPostLike.class);
		List<UserPostLike> postLikesList = (List<UserPostLike>)query.list();
		
		return postLikesList;
		
	}

	@Override
	public boolean isSessionExist(String userid) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from app_session where userid=?");
		query.setParameter(0,userid);
		query.addEntity(com.cs.spring.module.session.model.Session.class);
		  
		List<com.cs.spring.module.session.model.Session> returnSessionList =  (List<com.cs.spring.module.session.model.Session>)query.list();
		
		if(returnSessionList.size()>0)
		return true;
		else
			return false;
	}

	@Override
	public BigInteger getTotalLikes(int postid) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select count(postid) from app_user_post_like where postid=? and action=?");
		query.setParameter(0,postid);
		query.setParameter(1,1);
		//query.addEntity(Integer.class);
		List<Object> returnList =  (List<Object>)query.list();
		return  (BigInteger)returnList.get(0);
	}

	@Override
	public BigInteger getTotalDislikes(int postid) {
		
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select count(postid) from app_user_post_like where postid=? and action=?");
		query.setParameter(0,postid);
		query.setParameter(1,0);
		//query.addEntity(Integer.class);
		List<Object> returnList =  (List<Object>)query.list();
		return  (BigInteger)returnList.get(0);
	}

	@Override
	public BigInteger getTotalComments(int postid) {
		
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select count(commentid) from app_user_post_comment where postid=?");
		query.setParameter(0,postid);
		//query.addEntity(Integer.class);
		List<Object> returnList =  (List<Object>)query.list();
		return  (BigInteger)returnList.get(0);
	}

	@Override
	public UserPostResponse getNearbyPostById(int postid) {
		UserPostResponse userPost;
		
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from app_user_post where postid=?");
		query.setParameter(0,postid);
		query.addEntity(Post.class);
		Post nearbyPost = (Post) query.list().get(0);
		
			SQLQuery userQuery = session.createSQLQuery("select * from app_user where userid=?");
			userQuery.setParameter(0,nearbyPost.getUserid());
			userQuery.addEntity(User.class);
			
			userPost = new UserPostResponse();
			userPost.setPost(nearbyPost);
			userPost.setUser((User)userQuery.list().get(0));
			
		return userPost;
	}
	
	
		
	

}
