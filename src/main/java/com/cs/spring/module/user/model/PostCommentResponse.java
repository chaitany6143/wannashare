package com.cs.spring.module.user.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Proxy;


public class PostCommentResponse {
	
	PostComment postComment;
	User user;
	public PostComment getPostComment() {
		return postComment;
	}
	public void setPostComment(PostComment postComment) {
		this.postComment = postComment;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
