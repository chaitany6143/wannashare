package com.cs.spring.module.user.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author Chaitanya
 *
 */
@Entity
@Table(name = "app_user_post")
public class Post {

	//@ManyToOne(fetch = FetchType.EAGER)
	//@JoinColumn(name = "userid")
	private String userid;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int postid;

	private String post;

	private double latitude;

	private double longitude;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;
	
	
	/*//@JsonIgnore
	 public User getUser() {
	 return user_post;
	 }
	
	 public void setUser(User user) {
	 this.user_post = user;
	 }*/

	public int getPostid() {
		return postid;
	}

	public void setPostid(int postid) {
		this.postid = postid;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longtitude) {
		this.longitude = longtitude;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	

}
