package com.cs.spring.module.user.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cs.spring.module.user.model.PostComment;
import com.cs.spring.module.user.model.User;
import com.cs.spring.module.user.model.Post;
import com.cs.spring.module.user.model.UserPostResponse;
import com.cs.spring.module.user.model.UserPostImage;
import com.cs.spring.module.user.model.UserPostLike;

public interface UserDAO {

	public void addUser(User p);
	public void updateUser(User p);
	public List<User> listUsers();
	public User getUserById(String id);
	public void removeUser(int id);
	public boolean isUserExist(String userid);
	public int saveUserPost(Post userPost);
	public void saveUserPostImage(UserPostImage userPostImage);
	public List<Post> getUserPosts(String userid);
	public List<UserPostResponse> getNearbyPosts();
	public User getUserByPostId(int postid);
	public int savePostComment(PostComment postComment);
	public List<PostComment> getAllPostComments(String postid);
	public boolean addPostLike(UserPostLike postLike);
	public List<UserPostLike> getAllPostLikes(String postid);
	public boolean isSessionExist(String userid);
	public BigInteger getTotalLikes(int postid);
	public BigInteger getTotalDislikes(int postid);
	public BigInteger getTotalComments(int postid);
	public UserPostResponse getNearbyPostById(int postid);
}
