package com.cs.spring.module.user.controller;

import java.beans.DesignMode;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cs.spring.module.session.model.Session;
import com.cs.spring.module.session.service.SessionService;
import com.cs.spring.module.user.model.PostComment;
import com.cs.spring.module.user.model.PostCommentResponse;
import com.cs.spring.module.user.model.User;
import com.cs.spring.module.user.model.Post;
import com.cs.spring.module.user.model.UserPostImage;
import com.cs.spring.module.user.model.UserPostResponse;
import com.cs.spring.module.user.model.UserPostLike;
import com.cs.spring.module.user.service.UserService;
import com.cs.spring.utils.FileUtil;
import com.cs.spring.websockets.WebSocketPost;
import com.cs.spring.websockets.WebSocketSessionHandler;


@RestController
public class MainController {

	@Autowired
	private UserService userService;

	@Autowired
	private SessionService sessionService;

	Logger logger = Logger.getLogger(MainController.class.getName());

	/************************************ Session rest calls ***********************************************/
	@RequestMapping(value = "/user/{userid}/sessions", method = RequestMethod.GET)
	public List<Session> getSessionsByUserId(@PathVariable("userid") String userid) {

		//User user = this.userService.getUserById(Integer.parseInt(userid));
		// Set<Session> sessions =
		return this.sessionService.getSessionsByUserId(userid);

		//return user.getSessions();

	}

	// For add and update person both
	@RequestMapping(value = "/user/session", method = RequestMethod.POST)
	public boolean addUserSession(@RequestBody Session p) {

		//User user = this.userService.getUserById(Integer.parseInt(userid));
		//p.setUser(user);
		// user.getSessions().add(p);
		if (!this.userService.isSessionExist(p.getUserid())){ 
			this.sessionService.addSession(p);
		return true;
		}
		else
			return false;
	}

	@RequestMapping(value = "/{userid}/session/{sessionid}", method = RequestMethod.DELETE)
	public String removeSession(@PathVariable("userid") int userid,
			@PathVariable("sessionid") int sessionid, Model model) {

		this.sessionService.removeSession(userid);
		model.addAttribute("status", "Ok");
		return "jsonTemplate";
	}

	/******************************** User Rest calls *************************************************/

	@RequestMapping(value = "/user/{userid}", method = RequestMethod.GET)
	public User getUser(@PathVariable("userid") String userid) {
		return this.userService.getUserById(userid);
	}

	// For add and update person both
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public boolean addUser(@RequestBody User p) {

		
		if (this.userService.isUserExist(p.getUserid())) {
			return true;

		} else {
			this.userService.addUser(p);
			return true;
		}
		
	}

	@RequestMapping(value = "/user/{userid}", method = RequestMethod.DELETE)
	public String removePerson(@PathVariable("userid") int userid, Model model) {

		this.userService.removeUser(userid);
		model.addAttribute("status", "Ok");
		return "jsonTemplate";
	}

	@RequestMapping(value = "/user/post/{postid}/totalComments", method = RequestMethod.GET)
	public BigInteger getTotalLikes(@PathVariable("postid") int postid) {

		return this.userService.getTotalComments(postid);
	}
	/******************************** User Post Rest calls *************************************************/

	@RequestMapping(value = "/user/post", method = RequestMethod.POST)
	public String addPost(@RequestBody Post userPost) {
			String returnString=null;
		//User user = this.userService.getUserById(Integer.parseInt(userid));
		/* userPost.setUser(user);

		 */
			
			
			int postId= this.userService.saveUserPost(userPost);
			
			String userPostId = "Post:"+String.valueOf(userPost.getPostid());
			
			WebSocketSessionHandler.getInstance().sendToAll(userPostId);
		
		if(postId!=-1){
			returnString= String.valueOf(postId);
		}
		return returnString;

	}
	
	@RequestMapping(value = "/user/nearby/posts", method = RequestMethod.GET)
	public List<UserPostResponse> getNearbyPosts() throws IOException {

		List<UserPostResponse> nearbyUserPosts = this.userService.getAllNearbyPosts();
		
		
		
		for(UserPostResponse userPost : nearbyUserPosts){
			userPost.setThumbnaillocation((FileUtil.getFilesFromFolderTree(userPost.getUser().getUserid(),String.valueOf(userPost.getPost().getPostid()),"thumbnail")));
			userPost.setActuallocation((FileUtil.getFilesFromFolderTree(userPost.getUser().getUserid(),String.valueOf(userPost.getPost().getPostid()),"actual")));
			userPost.setTotallikes(this.userService.getTotalLikes(userPost.getPost().getPostid()).intValue());
			userPost.setTotaldisLikes(this.userService.getTotalDislikes(userPost.getPost().getPostid()).intValue());
			userPost.setTotalcomments(this.userService.getTotalComments(userPost.getPost().getPostid()).intValue());
		}

		/*for(UserPost post : nearbyPosts){
			User user = this.userService.getUserInfoByPostId(post.getPostid());
			post.setUser(user);
		}*/

		return nearbyUserPosts;
	}
	@RequestMapping(value = "/user/post/{postid}", method = RequestMethod.GET)
	public UserPostResponse getNearbyPostById(@PathVariable("postid") int postid) throws IOException {

		UserPostResponse userPost = this.userService.getNearbyPostById(postid);
		
			userPost.setThumbnaillocation((FileUtil.getFilesFromFolderTree(userPost.getUser().getUserid(),String.valueOf(userPost.getPost().getPostid()),"thumbnail")));
			userPost.setActuallocation((FileUtil.getFilesFromFolderTree(userPost.getUser().getUserid(),String.valueOf(userPost.getPost().getPostid()),"actual")));
			userPost.setTotallikes(this.userService.getTotalLikes(userPost.getPost().getPostid()).intValue());
			userPost.setTotaldisLikes(this.userService.getTotalDislikes(userPost.getPost().getPostid()).intValue());
			userPost.setTotalcomments(this.userService.getTotalComments(userPost.getPost().getPostid()).intValue());
		

		/*for(UserPost post : nearbyPosts){
			User user = this.userService.getUserInfoByPostId(post.getPostid());
			post.setUser(user);
		}*/

		return userPost;
	}
	/***************************************************User post image rest calls**************************************************************/

	
	@RequestMapping(value = "/images", method = RequestMethod.GET)
	public void getImagesByLocation(@RequestParam("fileLocation") String fileLocation,HttpServletResponse response) throws IOException {
		
			File file = new File(fileLocation);
			InputStream in = new FileInputStream(file);
			/*File outFile = new File(fileLocation.substring(0,fileLocation.lastIndexOf("/")+1)+"/"+file.getName().split("\\.")[0]+"-compressed."+file.getName().split("\\.")[1]);
			if(!outFile.exists())
				outFile.createNewFile();
			FileUtil.compressJpegFile(file, outFile,quality);*/
			// MIME type of the file
			response.setContentType("image/jpeg");
			//response.setContentType("application/octet-stream");
			long length = file.length();
			response.setHeader("Content-Length", String.valueOf(length));
			// Response header
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ file.getName() + "\"");
		      // copy it to response's OutputStream
			
		      org.apache.commons.io.IOUtils.copy(in, response.getOutputStream());
		     
		      response.flushBuffer();
		      
		
	}
	
	
	

	@RequestMapping(value = "/user/{userid}/post/{postid}/images", method = RequestMethod.POST)
	public String saveUserPostImage(@PathVariable("userid") String userid,@PathVariable("postid") String postid,@RequestPart("files") MultipartFile files[]) {
		String returnMsg = "Succesfully uploaded";
		String thumbnailFileDir = null;
		String actualFileDir = null;
		String destinationPath = FileUtil.createFolderTree(userid,postid);
		byte[] bytes=null;
		if(destinationPath!=null){
			destinationPath=destinationPath;
			thumbnailFileDir = destinationPath+"/thumbnail";
			actualFileDir = destinationPath+"/actual";
			FileUtil.createDirectories(thumbnailFileDir);
			FileUtil.createDirectories(actualFileDir);
			
			for (int i=0;i<files.length;i++) {
				if (!files[i].isEmpty()) {
					try {
						bytes = files[i].getBytes();
						String thumbnailFilePathLocation = thumbnailFileDir+"/"+files[i].getOriginalFilename();
						String actualFilePathLocation = actualFileDir+"/"+files[i].getOriginalFilename();
						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(actualFilePathLocation)));
						stream.write(bytes);
						stream.close();
						FileUtil.compressJpegFile(new File(actualFilePathLocation),new File(thumbnailFilePathLocation), 0.4F);
						UserPostImage userPostImage = new UserPostImage();
						userPostImage.setPostid(postid);
						userPostImage.setActualloc(actualFilePathLocation);
						userPostImage.setThumbnailloc(thumbnailFilePathLocation);
						userPostImage.setFilename(files[i].getOriginalFilename());
						this.userService.saveUserPostImage(userPostImage);
						
					} catch (IOException e) {
						returnMsg=e.getLocalizedMessage();
						
					}


				}
				else
					returnMsg="Uploading failed";

			}

			return "";
		}


		//this.userService.saveUserPostImage(userPostImage);

		//model.addAttribute("status", "Ok");

		return returnMsg.toString();
	}
	
	/***************************************************User post comment rest calls**************************************************************/
	
	@RequestMapping(value = "/user/post/comment", method = RequestMethod.POST)
	public String addPostComment(@RequestBody PostComment postComment) {
		String returnString=null;
		
			int commentId= this.userService.savePostComment(postComment);
			String message = "Comment:"+postComment.getPostid();
			WebSocketSessionHandler.getInstance().sendToAll(message);
		
		if(commentId!=-1){
			returnString= String.valueOf(commentId);
		}
		return returnString;
	}
	@RequestMapping(value = "/user/post/{postid}/comments", method = RequestMethod.GET)
	public List<PostComment> getPostComments(@PathVariable("postid") String postid) {
		
		List<PostComment> returnList = new ArrayList<>();
	
		List<PostComment> postComments = this.userService.getAllPostComments(postid);
		
		return postComments;
		
	}
	@RequestMapping(value = "/user/post/like", method = RequestMethod.POST)
	public boolean addPostLike(@RequestBody UserPostLike postLike){
		String message = "Comment:"+postLike.getPostid();
		WebSocketSessionHandler.getInstance().sendToAll(message);
		
		return this.userService.addPostLike(postLike);
		
		
	}
	@RequestMapping(value = "/user/post/{postid}/likes", method = RequestMethod.GET)
	public List<UserPostLike> getPostLikes(@PathVariable("postid") String postid) {
		
		List<UserPostLike> postLikes = this.userService.getAllPostLikes(postid);
		return postLikes;
		
	}
	@RequestMapping(value = "/app/download", method = RequestMethod.GET)
	public void downloadApp(HttpServletResponse response) throws IOException {
		

		File file = new File("/Users/chaitanya/Downloads/dev/AndroidWork/workspace/WannaShare/app/build/outputs/apk/app-debug.apk");
		//File file = new File("/home/ec2-user/app-debug.apk");
		InputStream in = new FileInputStream(file);
		
		response.setContentType("application/vnd.android.package-archive");
		//response.setContentType("application/octet-stream");
		long length = file.length();
		response.setHeader("Content-Length", String.valueOf(length));
		// Response header
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ file.getName() + "\"");
	      // copy it to response's OutputStream
		
	      org.apache.commons.io.IOUtils.copy(in, response.getOutputStream());
	     
	      response.flushBuffer();
		
		
	}

}

