package com.cs.spring.module.user.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.spring.module.user.dao.UserDAO;
import com.cs.spring.module.user.model.PostComment;
import com.cs.spring.module.user.model.User;
import com.cs.spring.module.user.model.Post;
import com.cs.spring.module.user.model.UserPostResponse;
import com.cs.spring.module.user.model.UserPostImage;
import com.cs.spring.module.user.model.UserPostLike;


@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;
	
	

	public void setuserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	@Transactional
	public void addUser(User p) {
		this.userDAO.addUser(p);
	}

	@Override
	@Transactional
	public void updateUser(User p) {
		this.userDAO.updateUser(p);
	}

	@Override
	@Transactional
	public List<User> listUsers() {
		return this.userDAO.listUsers();
	}

	@Override
	@Transactional
	public User getUserById(String id) {
		return this.userDAO.getUserById(id);
	}

	@Override
	@Transactional
	public void removeUser(int id) {
		this.userDAO.removeUser(id);
	}

	

	@Override
	@Transactional
	public boolean isUserExist(String userid) {
		
		return this.userDAO.isUserExist(userid);
	}

	

	@Override
	@Transactional
	public int saveUserPost(Post userPost) {
		return this.userDAO.saveUserPost(userPost);
		
	}

	@Override
	@Transactional
	public void saveUserPostImage(UserPostImage userPostImage) {
		this.userDAO.saveUserPostImage(userPostImage);
		
	}

	@Override
	@Transactional
	public List<Post> getUserPosts(String userid) {
		
		return this.userDAO.getUserPosts(userid);
	}

	@Override
	@Transactional
	public List<UserPostResponse> getAllNearbyPosts() {
		
		return this.userDAO.getNearbyPosts();
	}

	@Override
	@Transactional
	public User getUserInfoByPostId(int postid) {
		// TODO Auto-generated method stub
		return this.userDAO.getUserByPostId(postid);
	}

	@Override
	@Transactional
	public int savePostComment(PostComment postComment) {
		
		return this.userDAO.savePostComment(postComment);
	}

	@Override
	@Transactional
	public List<PostComment> getAllPostComments(String postid) {
		// TODO Auto-generated method stub
		return this.userDAO.getAllPostComments(postid);
	}

	@Override
	@Transactional
	public boolean addPostLike(UserPostLike postLike) {
		
		return this.userDAO.addPostLike(postLike);
	}

	@Override
	@Transactional
	public List<UserPostLike> getAllPostLikes(String postid) {
		
		return this.userDAO.getAllPostLikes(postid);
	}

	@Override
	@Transactional
	public boolean isSessionExist(String userId) {
		
			return this.userDAO.isSessionExist(userId);
		}

	@Override
	@Transactional
	public BigInteger getTotalLikes(int postid) {
		
		return this.userDAO.getTotalLikes(postid);
	}

	@Override
	@Transactional
	public BigInteger getTotalDislikes(int postid) {
		
		return this.userDAO.getTotalDislikes(postid);
	}

	@Override
	@Transactional
	public BigInteger getTotalComments(int postid) {
		
		return this.userDAO.getTotalComments(postid);
	}

	@Override
	@Transactional
	public UserPostResponse getNearbyPostById(int postid) {
		return this.userDAO.getNearbyPostById(postid);
	}
	}

	


