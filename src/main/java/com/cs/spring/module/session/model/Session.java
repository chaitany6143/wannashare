package com.cs.spring.module.session.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.cs.spring.module.user.model.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author Chaitanya
 *
 */
@Entity
@Table(name = "app_session")
@Proxy(lazy = false)
public class Session {

	
	@Id
	@Column(name = "sessionid")
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private int sessionid;

	//@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name = "userid")
	private String userid;

	private String token;

	public int getSessionid() {
		return sessionid;
	}

	public void setSessionid(int sessionid) {
		this.sessionid = sessionid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	



}
