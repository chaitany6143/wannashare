package com.cs.spring.module.session.dao;


import java.util.List;
import java.util.Set;

import com.cs.spring.module.session.model.Session;



public interface SessionDao {

	public void addSession(Session p);

	public void updateSession(Session p);


	public void removeSession(int userid);

	public boolean isSessionExists(String userid);

	public List<Session> getSessionsByUserId(String userid);

	
}
