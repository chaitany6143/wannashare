package com.cs.spring.module.session.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cs.spring.module.session.model.Session;
import com.cs.spring.module.user.model.User;

@Repository
public class SessionDaoImpl implements SessionDao {

	private static final Logger logger = LoggerFactory
			.getLogger(SessionDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addSession(Session p) {
		org.hibernate.Session session = this.sessionFactory.getCurrentSession();

		session.saveOrUpdate(p);

		logger.info("Session saved successfully, Session Details=" + p);

	}

	@Override
	public void updateSession(Session p) {
		org.hibernate.Session session = this.sessionFactory.getCurrentSession();
		// User pp = (User) session.load(User.class, new Integer(p.getUser()
		// .getUserid()));
		// // p.setUser(pp);
		session.update(p);
		logger.info("Session updated successfully, Session Details=" + p);

	}

	@Override
	public List<Session> getSessionsByUserId(String userid) {
		org.hibernate.Session session = this.sessionFactory.getCurrentSession();

		//User p = (User) session.get(User.class, new String(userid));
		SQLQuery query = session.createSQLQuery("select * from app_session where userid=?");
		query.setParameter(0,userid);
		query.addEntity(Session.class);
		List<Session> sessionList = query.list();

		logger.info("Session loaded successfully");
		return sessionList;
	}

	@Override
	public void removeSession(int userid) {
		org.hibernate.Session session = this.sessionFactory.getCurrentSession();
		Session p = (Session) session.load(Session.class, new Integer(userid));
		if (null != p) {
			session.delete(p);
		}
		logger.info("Session deleted successfully, Session details=" + p);

	}

	@Override
	public boolean isSessionExists(String userid) {
		org.hibernate.Session session = this.sessionFactory.getCurrentSession();
		Query q = session.createQuery("from Session where userid = :userid");
		q.setParameter("userid", userid);
		Session e = (Session) q.uniqueResult();
		if (e == null)
			return false;
		else
			return true;
	}

}
