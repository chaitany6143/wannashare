package com.cs.spring.module.session.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.cs.spring.module.session.model.Session;
import com.cs.spring.module.user.dao.UserDAO;
import com.cs.spring.module.user.dao.UserDAOImpl;
import com.cs.spring.module.user.model.User;
import com.cs.spring.module.user.service.UserService;
import com.cs.spring.module.user.service.UserServiceImpl;


public interface SessionService {

	public void addSession(Session p);
	public void updateSession(Session p);
	public void removeSession(int userid);
	public boolean isSessionExists(String userid);
	public List<Session> getSessionsByUserId(String userid);
	
	
}
