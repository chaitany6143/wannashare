package com.cs.spring.module.session.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.cs.spring.module.session.dao.SessionDao;
import com.cs.spring.module.session.model.Session;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SessionServiceImpl implements SessionService{
	
	@Autowired
	private SessionDao sessionDao;

	@Override
	@Transactional
	public void addSession(Session p) {
		this.sessionDao.addSession(p);
	}

	@Override
	@Transactional
	public void updateSession(Session p) {
		this.sessionDao.updateSession(p);
		
	}

	@Override
	@Transactional
	public List<Session> getSessionsByUserId(String userid) {
		return this.sessionDao.getSessionsByUserId(userid);
		
	}

	@Override
	@Transactional
	public void removeSession(int userid) {
		this.sessionDao.removeSession(userid);
		
	}

	@Override
	@Transactional
	public boolean isSessionExists(String userid) {
		 return this.sessionDao.isSessionExists(userid);
		
	}

	

	

}
