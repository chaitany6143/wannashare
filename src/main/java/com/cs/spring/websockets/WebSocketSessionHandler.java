package com.cs.spring.websockets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.cs.spring.module.user.model.UserPostResponse;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WebSocketSessionHandler {

	  private static WebSocketSessionHandler instance = null;
	  private static Map<String,WebSocketSession> userSessionMap;
	  private static Gson gson;
	  
	  protected WebSocketSessionHandler() {
		  userSessionMap = new HashMap<>();
		   gson = new Gson();
		  
		//Second way to create a Gson object using GsonBuilder
		 
		 gson = new GsonBuilder()
		             .disableHtmlEscaping()
		             .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
		             .setPrettyPrinting()
		             .serializeNulls()
		             .create();
		   
	   }
	   public static WebSocketSessionHandler getInstance() {
	      if(instance == null) {
	         instance = new WebSocketSessionHandler();
	      }
	      return instance;
	   }
	   
	   public static void addOrUpdateSession(String key,WebSocketSession value){
		/*  if(!userSessionMap.containsKey(key))
		   userSessionMap.put(key, value);  
		   
	   else if(!userSessionMap.get(key).isOpen()){*/
		   userSessionMap.put(key, value);
	  // }
	   }
	   public static WebSocketSession getSessionFromId(String key){
		   if(userSessionMap.containsKey(key))
			   return userSessionMap.get(key);
		   
		   return null;
	   }
	   
	   public static List<WebSocketSession> getAllOpenSessions(){
		  List<WebSocketSession> sessions = new ArrayList<>();
		  
		  for(WebSocketSession session : userSessionMap.values()){
			  
			  if(session.isOpen())
				  sessions.add(session);
		  }
		   
		   return sessions;
	   }
	public static void sendToAll(String text)  {
		
		List<WebSocketSession> openSessions = getAllOpenSessions();
		for(WebSocketSession session : openSessions){
			
			try {
				session.sendMessage(new TextMessage(text));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	   
	
}
